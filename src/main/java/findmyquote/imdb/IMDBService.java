package findmyquote.imdb;

import com.amazonaws.services.lambda.invoke.LambdaFunction;

import java.util.ArrayList;

public interface IMDBService {
    @LambdaFunction(functionName="quoteSearch")
    ArrayList<Result> quoteSearch(Query input);
}
